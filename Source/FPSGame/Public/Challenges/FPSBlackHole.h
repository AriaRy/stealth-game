// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Actor.h"
#include "FPSBlackHole.generated.h"

class USphereComponent;

UCLASS()
class FPSGAME_API AFPSBlackHole : public AActor
{
    GENERATED_BODY()

    UPROPERTY(VisibleAnywhere, Category = "Components")
    UStaticMeshComponent* MeshComponent;

    UPROPERTY(VisibleAnywhere, Category = "Components")
    USphereComponent* InnerSphereComponent;

    UPROPERTY(VisibleAnywhere, Category = "Components")
    USphereComponent* OuterSphereComponent;

public:
    // Sets default values for this actor's properties
    AFPSBlackHole();

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    UFUNCTION()
    void OnOverlapInnerSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                              UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep,
                              const FHitResult& SweepResult);

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;
};
