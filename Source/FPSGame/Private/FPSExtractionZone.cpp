// Fill out your copyright notice in the Description page of Project Settings.


#include "FPSExtractionZone.h"

#include "FPSCharacter.h"
#include "FPSGameMode.h"
#include "Components/BoxComponent.h"
#include "Components/DecalComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFPSExtractionZone::AFPSExtractionZone()
{
    OverlapComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("OverlapComponent"));
    OverlapComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    OverlapComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
    OverlapComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
    OverlapComponent->SetBoxExtent(FVector(200.0f));
    RootComponent = OverlapComponent;

    OverlapComponent->OnComponentBeginOverlap.AddDynamic(this, &AFPSExtractionZone::HandleOverlap);

    DecalComponent = CreateDefaultSubobject<UDecalComponent>(TEXT("DecalComponent"));
    DecalComponent->DecalSize = FVector(200.0f, 200.0f, 200.0f);
    DecalComponent->SetupAttachment(OverlapComponent);
}

void AFPSExtractionZone::HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                       UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                       const FHitResult& SweepResult)
{
    AFPSCharacter* MyPawn = Cast<AFPSCharacter>(OtherActor);

    if (!MyPawn) return;

    if (MyPawn->bIsCarryingObjective)
    {
        AFPSGameMode* GameMode = Cast<AFPSGameMode>(GetWorld()->GetAuthGameMode());

        if (GameMode)
        {
            GameMode->CompleteMission(MyPawn, true);
        }
    }
    else
    {
        UGameplayStatics::PlaySound2D(this, ObjectiveMissingSound);
    }
}
