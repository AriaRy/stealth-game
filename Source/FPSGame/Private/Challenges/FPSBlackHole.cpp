// Fill out your copyright notice in the Description page of Project Settings.


#include "Challenges/FPSBlackHole.h"

#include "Components/SphereComponent.h"

// Sets default values
AFPSBlackHole::AFPSBlackHole()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
    MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    RootComponent = MeshComponent;

    InnerSphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("InnerSphereComponent"));
    InnerSphereComponent->SetSphereRadius(100);
    InnerSphereComponent->SetupAttachment(MeshComponent);
    InnerSphereComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    

    OuterSphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("OuterSphereComponent"));
    OuterSphereComponent->SetSphereRadius(3000);
    OuterSphereComponent->SetupAttachment(MeshComponent);
}

// Called when the game starts or when spawned
void AFPSBlackHole::BeginPlay()
{
    Super::BeginPlay();

    InnerSphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AFPSBlackHole::OnOverlapInnerSphere);
}

void AFPSBlackHole::OnOverlapInnerSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                      UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep,
                                      const FHitResult& SweepResult)
{
    if (OtherActor)
    {
        OtherActor->Destroy();
    }
}


// Called every frame
void AFPSBlackHole::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    TArray<UPrimitiveComponent*> OverlappingComponents;
    OuterSphereComponent->GetOverlappingComponents(OverlappingComponents);

    for (UPrimitiveComponent* OverlappingComponent : OverlappingComponents)
    {
        if (OverlappingComponent && OverlappingComponent->IsSimulatingPhysics())
        {
            const float SphereRadius = OuterSphereComponent->GetScaledSphereRadius();
            const float ForceStrength = -2000;

            OverlappingComponent->AddRadialForce(GetActorLocation(), SphereRadius, ForceStrength, RIF_Constant, true);
        }
    }
}
